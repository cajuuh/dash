# Dash!
Source code for a dashboard for a clinic.

*Developed on SL7 by @cajuuh and @edvaldo.azevedo*

---------

Actually on:

![mariá dermato funcional](https://gitlab.com/cajuuh/dash/raw/748e927a5b36ed5e2ea4bf24b14f910aa833696a/fisiosistema/src/main/webapp/assets/img/logoNatanaara.png "Mariá Dermato Funcional")

---------
TODO list so far:

- [x] Change language to pt-br
- [x] Change users cards (waiting for approval)
- [x] Create a query reader on users page
- [x] Create a query reader on users consultation assignment dialog
- [x] Change to material design
- [x] Add expenses form
- [x] Add investments form
- [ ] Add Relatorios button to new template
- [ ] Add sliding downbar to Relatorios button

---------

Trello:


<a href="https://trello.com/b/i11vQJ5m/sysfisio"><img src="https://www.shareicon.net/data/128x128/2015/10/17/657501_logo_512x512.png"/></a>