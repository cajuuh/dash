app.config(function($routeProvider, $locationProvider) {
	$routeProvider.when('/login', {
		templateUrl : 'login.html',
		controller : 'loginCtrl'
	}).when('/index', {
		templateUrl : 'index.html',
		controller : 'indexCtrl'
	}).when('/dashboard', {
		templateUrl : 'dashboard.html',
		controller : 'dashboardcontroller'
	}).when('/paciente', {
		templateUrl : 'user.html',
		controller : 'pacientecontroller'
	}).when('/calendar', {
		templateUrl : 'table.html',
		controller : 'calendarcontroller'
	}).otherwise({
		redirectTo : '/login'
	});
});



/*
service definido para salvar pacientes entre um form e outro
 */
app.factory('myService', function() {
	var savedData = {}
	var savedServicos = []
	function set(data) {
		savedData = data;
	}
	function get() {
		return savedData;
	}
	function addToServicos(data) {
		savedServicos.push(data)
	}
	function getServicos() {
		return savedServicos;
	}
	return {
		set : set,
		get : get,
		addToServicos : addToServicos,
		getServicos : getServicos
	}
});

app.factory('pacienteService', function() {

    var pacienteSavedData = {}

    function set(data) {
        pacienteSavedData = data;
    }
    function get() {
        return pacienteSavedData;
    }
    return {
        set : set,
        get : get,
    }
})

app.factory('Data', function() {
	var data = {
		selectedNome : ''
	};
	return {
		getFirstName : function() {
			return data.selectedNome;
		},
		setFirstName : function(firstName) {
			data.selectedNome = firstName;
		}
	};
});

app.controller('maincontroller', ['$scope', '$mdDialog', '$http', function($scope, $mdDialog, $http, $location) {
	$scope.title = "Mariá Dermato Funcional";
	$scope.getInterest1 = function() {
		var results = $scope.pacientes;
		return results;
	};
	$scope.logout = function(){
		$http({
			method:'POST',
			url:'/logout'
		}).success(function(){
			$location.path('/login')
		});
	}
}]);

app.controller('pacientecontroller', ['$scope','$http','pacienteService', '$mdDialog',function($scope,$http,$pacienteService,$mdDialog) {

	$scope.title = "Mariá Dermato Funcional";

	$scope.editingPaciente = $pacienteService.get();

	$scope.pacientes = [];

	$http({
		method : 'GET',
		url : '/paciente'
	}).success(function(response) {
		$scope.pacientes = response;
	}).error(function(status) {
		console.log(status);
	});

	$scope.deletarPaciente = function(paciente){
		console.log(paciente);
		$http({
			method:'DELETE',
			url:"/paciente/"+paciente.id
		}).success(function(){
			$scope.paciente.splice(paciente.id, 1);
		}).error(function(){

		});
	}
	
	$scope.editarPaciente = function(editingPaciente){
		$http({
			method:'PUT',
			url:'/paciente',
			data:editingPaciente
		}).success(function(){
			
		}).error(function(){
			
		});
	}
	$scope.editaPaciente = function (paciente) {
        $pacienteService.set(paciente);
    }

    // Dialog edit paciente sign-up
    $scope.showAdvanced5 = function(ev) {
        $mdDialog.show({
            controller : DialogController,
            templateUrl : 'form5.tmpl.html',
            parent : angular.element(document.body),
            targetEvent : ev,
            clickOutsideToClose : true,
            fullscreen : $scope.customFullscreen
            // Only for -xs, -sm breakpoints.
        });
    };

    function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

}]);

app.controller('dashboardcontroller', function($scope, $http) {
    $scope.title = "Mariá Dermato Funcional";
	$scope.consultas = [];
	$http({
		method : 'GET',
		url : '/agendamento'
	}).then(function successCallback(response) {
		$scope.consultas = response.data;
		console.log($scope.consultas);
	}, function errorCallback(response) {
	});
	
	
});

app.controller('calendarcontroller', function CalendarCtrl($scope, $filter, $compile, uiCalendarConfig, $mdDialog, $http) {
    $scope.title = "Mariá Dermato Funcional";
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	var myCalendar = uiCalendarConfig.calendars.myCalendar;
	// Mexer aqui!!!!
	/*
	 * Requisição Agendamentos realiza forEach no response e
	 * adiciona no $scope de eventos que será exibido no
	 * Calendar
	 */
	$scope.events = [];
	$http({
		method : 'GET',
		url : '/agendamento'
	}).then(function(response) {
		angular.forEach(response.data, function(value, key) {
			$scope.events.push({
				title : "Paciente : " + value.paciente.nome +"\n" + "Serviço :"+ value.servico.descricao,
				start : value.data + "T" + value.hora + "-0300",
				anotacao : value.anotacao,
				stick : true
			});
		});
	});
	/* Atribui eventos ao $scope de EventsSources */
	$scope.eventSources = [ $scope.events ];
	/* alert on eventClick */
	$scope.alertOnEventClick = function(date, jsEvent, view) {
		$scope.alertMessage = (date.title + ' was clicked ');
	};
	/* alert on Drop */
	$scope.alertOnDrop = function(event, delta, revertFunc,
			jsEvent, ui, view) {
		$scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
	};
	/* alert on Resize */
	$scope.alertOnResize = function(event, delta, revertFunc,
			jsEvent, ui, view) {
		$scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
	};
	/* add and removes an event source of choice */
	/* Change View */
	$scope.renderCalender = function(calendar) {
		if (uiCalendarConfig.calendars[calendar]) {
			uiCalendarConfig.calendars[calendar]
					.fullCalendar('render');
		}
	};
	/* Render Tooltip */
	$scope.eventRender = function(event, element, view) {
		element.attr({
			'tooltip' : event.title,
			'tooltip-append-to-body' : true
		});
		$compile(element)($scope);
	};
	var splitTime = function(timesplitter) {
		timesplitter.split()
	};
	$scope.setCalDate = function(date, jsEvent, view) {
		var selectedDate = moment(date).format('YYYY-MM-DD'); // set dateFrom based on user click on calendar
		var log = [];
		var substring;
		var title;
		//$scope.selectedDate = $filter('date')(selectedDate, 'yyyy-MM-dd'); // update $scope.dateFrom
		// console.log('$scope.uiConfig', $scope.uiConfig);
		// console.log('uiCalendarConfig', uiCalendarConfig);
		angular.forEach($scope.events, function(value, key) {
			substring = value.start.substring(0,10);
			if (substring == selectedDate)
				log.push(value.title);
		});
		if (log[0] == null) {
			console.log("");
		}
		else {
			$mdDialog.show({
						parent : angular.element(document.body),
						targetEvent : jsEvent,
						template : '<md-dialog aria-label="List dialog">'
								+ '  <md-dialog-content>'
								+ '    <md-list>'
								+ '      <md-list-item ng-repeat="item in log">'
								+ '       <div>{{item}}</div>'
								+ '    </md-list-item>'
								+ '   </md-list>'
								+ '  </md-dialog-content>'
								+ '  <md-dialog-actions>'
								+ '    <md-button ng-click="closeDialog()" class="md-primary">'
								+ '      Ok'
								+ '    </md-button>'
								+ '  </md-dialog-actions>'
								+ '</md-dialog>',
						locals : {
							log : log
						},
						controller : DialogController
					});
			function DialogController($scope, $mdDialog, log) {
					$scope.log = log;
					$scope.closeDialog = function() {
					$mdDialog.hide();
				}
			}
		}
	};
	$scope.showAlert = function(date, jsEvent, view) {
		// Appending dialog to document.body to cover sidenav in
		// docs app
		// Modal dialogs should fully cover application
		// to prevent interaction outside of dialog
		$mdDialog.show($mdDialog.alert().clickOutsideToClose(
			true)
			.title(date.title)
			.textContent(date.start.format('h:mmA'))
			.ariaLabel('Event Dialog')
			.ok('Ok')
			.targetEvent(jsEvent));
	};
	/* config object */
	$scope.uiConfig = {
		calendar : {
			lang : 'pt-br',
			height : 550,
			width : 500,
			aspectRatio : 200,
			firstDay : 1,
			editable : true,
			header : {
				left : 'title',
				center : ''
			},
			dayClick : $scope.setCalDate,
			eventClick : $scope.showAlert,
			eventDrop : $scope.alertOnDrop,
			eventResize : $scope.alertOnResize,
			eventRender : $scope.eventRender
		}
	};
});

app.controller('loginCtrl', function($scope, $http, $rootScope,$location) {
	
	 
	// Realizar Autenticação do Usuário
	$scope.autenticar = function(usuario) {
		console.log(usuario.nome);
		$http({
			method : 'GET',
			url : '/login',
			data:usuario
		}).success(function(response) {
			$location.path("/dashboard");
		}).error(function(response) {
			$location.path("/login");
		      
		});
	}
});

app.controller('formscontroller', ['$scope', '$mdDialog', '$http', 'myService', 'Data', function($scope, $mdDialog, $http, $myService, $Data) {
	$scope.results = [];
	$scope.reset = {};
	$scope.paciente = $myService.get();

	$scope.savedPaciente = true;
	$scope.savedAgendamento = true;

    $scope.findAgendamentoPacientes = [];

	$scope.buscarEndereco = function(paciente) {
		$scope.dadosEndereco = [];
		var cep = paciente.endereco.cep;
		console.log(cep);
		$http({
			method : 'GET',
			url : 'https://viacep.com.br/ws/' + cep + '/json/',
			responseType : 'json'
		}).success(function(response) {
			$scope.paciente.endereco = response;
			console.log($scope.dadosEndereco);
		}).error(function() {
		});
	}
	// Busca Paciente P/Nome
	$scope.buscarPaciente = function(nome) {
		// var nome = consulta.paciente.nome;
		$scope.findPacientes = [];
		console.log(nome);
		$http({
			method : 'GET',
			url : '/paciente/nome/' + nome,
		}).success(function(response) {
			console.log(response);
			$scope.findPacientes = response;
			console.log($scope.findPacientes);
		}).error(function() {
		});
	}

	$scope.loadComplete = function(nome) {
		$http.get("/paciente/nome/" + nome).then(function(response) {
			$scope.results = response;
		});
		// return promise;
	}

	$scope.loadCompleteAgendamento = function(nome) {
		$http.get("/agendamento/nome/" + nome).then(function(response) {
			$scope.results = response;
		});
		// return promise;
	}

	$scope.perPaciente = function(data) {
		if (data == null) {
			console.log('vazio');
		} else {
			$myService.set(data);
		}
	}

	$scope.limparFormAgendamento = function () {
		$scope.agendamento = '';
    }

	$scope.agendarConsulta = function(agendamento) {
		$scope.agendamento.paciente = $scope.paciente;
		$http({
			method : 'POST',
			url : '/agendamento',
			data : agendamento,
		}).success(function(response) {
			//alert("deu bom");
            $scope.savedAgendamento;
		}).error(function(response) {
			var msg = response;
			alert(msg);
            $scope.savedAgendamento = false;
		});
		$scope.limparFormAgendamento();
	}
	$scope.salvarPaciente = function(paciente) {
		console.log(paciente);
		$http({
			method : 'POST',
			url : '/paciente',
			data : paciente
		}).then(function successCallback(response) {
			//alert("Cliente Cadastrado :" + paciente.nome);
            $scope.savedPaciente;
		}, function errorCallback(response) {
			//alert("Paciente Com Cpf já Cadastrado");
            $scope.savedPaciente = false;
		});
		$scope.pacienteForm.$setPristine();
	}
	
	$scope.status = '  ';
	$scope.customFullscreen = false;
	// Dialog pacient search
	$scope.showAdvanced = function(ev) {
		$mdDialog.show({
			controller : DialogController,
			templateUrl : 'form1.tmpl.html',
			parent : angular.element(document.body),
			targetEvent : ev,
			clickOutsideToClose : true,
			fullscreen : $scope.customFullscreen
		// Only for -xs, -sm breakpoints.
		});
	};
	// Dialog pacient sign-up
	$scope.showAdvanced2 = function(ev) {
		$mdDialog.show({
			controller : DialogController,
			templateUrl : 'form2.tmpl.html',
			parent : angular.element(document.body),
			targetEvent : ev,
			clickOutsideToClose : true,
			fullscreen : $scope.customFullscreen
		// Only for -xs, -sm breakpoints.
		});
	};

	// Dialog appointment
	$scope.showAdvanced3 = function(ev) {
		$mdDialog.show({
			controller : DialogController,
			templateUrl : 'form3.tmpl.html',
			parent : angular.element(document.body),
			targetEvent : ev,
			clickOutsideToClose : true,
			fullscreen : $scope.customFullscreen
		// Only for -xs, -sm breakpoints.
		});
	};

	// "Controller" to custom dialog
	function DialogController($scope, $mdDialog) {
		$scope.hide = function() {
			$mdDialog.hide();
		};
		$scope.cancel = function() {
			$mdDialog.cancel();
		};
		$scope.answer = function(answer) {
			$mdDialog.hide(answer);
		};
	}
	
	//-------Buscar Servicos Backend--------
	$scope.buscarServicos = function(){
		$scope.servicos = [];
		$http({
			method:'GET',
			url:'/servico'
		}).success(function(response){
			$scope.registroServicos = response;
		}).error(function(){
			alert ("sem servico");
		});
	}
	
	// ------ Type Phone Picker ------
	$scope.phoneTypes = [ 'residencial', 'celular', 'comercial' ];
	$scope.selectedPhoneType;
	$scope.getSelectedPhoneType = function() {
		if ($scope.selectedPhoneType !== undefined) {
			return $scope.selectedPhoneType;
		} else {
			return "Por favor selecione o tipo de telefone";
		}
	}
}]);
app.controller('servicescontroller', [ '$scope','$http','$log', function($scope,$http,$log,$mdDialog) {
	$scope.title = "Mariá Dermato Funcional";
    $scope.cadastros = ['Serviços', 'Produtos', 'Pessoas', 'Consultas', 'Despesas', 'Investimentos', 'Lançamentos'];

    $scope.cadastrarServico = function(servico){
    	$http({
    		method:'POST',
    		url:'/servico',
    		data:servico
    	}).then(function successCallback(response) {
    		
    	}, function errorCallback(response) {
    	});
    }
	
}]).config(
	function($mdThemingProvider) {
	$mdThemingProvider.theme('docs-dark', 'default').primaryPalette('yellow').dark();
})