var indexApp = angular.module("indexApp", [ 'ngMaterial', 'ngMessages',
		'$scope', '$mdDialog', '$http' ])

.controller('mainCtrl', function($scope, $http) {
	// Datepicker
	$scope.myDate = new Date();

	$scope.minDate = new Date($scope.myDate.getFullYear(), $scope.myDate
			.getMonth() - 2, $scope.myDate.getDate());

	$scope.maxDate = new Date($scope.myDate.getFullYear(), $scope.myDate
			.getMonth() + 2, $scope.myDate.getDate());

	$scope.onlyWeekendsPredicate = function(date) {
		var day = date.getDay();
		return day === 0 || day === 6;
	};
	// ------

	// Dialog Popup
	$scope.status = '  ';
	$scope.customFullscreen = false;
	$scope.showPrompt = function(ev) {
		// Appending dialog to document.body to cover sidenav in docs
		// app
		var confirm = $mdDialog.prompt().title('What would you name your dog?')
				.textContent('Bowser is a common name.')
				.placeholder('Dog name').ariaLabel('Dog name').initialValue(
						'Buddy').targetEvent(ev).ok('Okay!').cancel(
						'I\'m a cat person');

		$mdDialog.show(confirm).then(function(result) {
			$scope.status = 'You decided to name your dog ' + result + '.';
		}, function() {
			$scope.status = 'You didn\'t name your dog.';

		});
	};

	$scope.showAdvanced = function(ev) {
		$mdDialog.show({
			controller : DialogController,
			templateUrl : 'form1.tmpl.html',
			parent : angular.element(document.body),
			targetEvent : ev,
			clickOutsideToClose : true,
			fullscreen : $scope.customFullscreen
		// Only for -xs, -sm breakpoints.
		}).then(function(answer) {
			$scope.status = 'You said the information was "' + answer + '".';
		}, function() {
			$scope.status = 'You cancelled the dialog.';
		});
	};

	$scope.showAdvanced2 = function(ev) {
		$mdDialog.show({
			controller : DialogController,
			templateUrl : 'form2.tmpl.html',
			parent : angular.element(document.body),
			targetEvent : ev,
			clickOutsideToClose : true,
			fullscreen : $scope.customFullscreen
		// Only for -xs, -sm breakpoints.
		}).then(function(answer) {
			$scope.status = 'You said the information was "' + answer + '".';
		}, function() {
			$scope.status = 'You cancelled the dialog.';
		});
	};

	function DialogController($scope, $mdDialog) {
		$scope.hide = function() {
			$mdDialog.hide();
		};

		$scope.cancel = function() {
			$mdDialog.cancel();
		};

		$scope.answer = function(answer) {
			$mdDialog.hide(answer);
		};
	}
})
.controller('pacienteCtrl', function($scope,$http){
	
})
.controller('modalConsultaCtrl',function($scope,$http){
	// Busca Paciente P/Nome
	$scope.buscarPaciente = function(agendamento) {
		cosole.log(agendamento.paciente);
		var paciente = agendamento.paciente;
		$http({
			method : 'GET',
			url : '/paciente/',
			paciente: paciente
		}).then(function successCallback(response) {
			console.log(response);
			

		}, function errorCallback(response) {

		});

	}
});


