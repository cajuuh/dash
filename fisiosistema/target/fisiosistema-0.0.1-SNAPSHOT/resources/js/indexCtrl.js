var indexApp= angular.module('indexApp',['ngRoute','ui.bootstrap'])

//Direcionamento de Páginas
.config(function($routeProvider,$locationProvider){
	$routeProvider
		.when('/login',{
			templateUrl:'login.html',
			controller:'loginCtrl'
			
		})
		.when('/index',{
			templateUrl:'index.html',
			controller:'indexCtrl'
		})
		.when('/home',{
			templateUrl:'home.html',
			controller:'homeCtrl'
		})
		.when('/pct',{
			templateUrl:'pct.html',
			controller:'pacienteCtrl'
		})
		.when('/calendar',{
			templateUrl:'calendar.html',
			controller:'calendarCtrl'
		})
		.otherwise({ redirectTo: '/login'});
	 
		
})
.controller('loginCtrl',function($scope,$http,$location){
	//Realizar Autenticação do Usuário
	$scope.autenticar = function(usuario){
		console.log(usuario.nome);
		
		$http({
			method: 'POST',
			url:'/autenticar',
			data:usuario
		}).success(function(){
			$location.path('/dashboard');
			
		}).error(function(response){
			
			$location.path('/login');
			
		});
	}
	
})
.controller('homeCtrl',function($scope){
	
	$scope.agendamentos =[
	                   {"dia":"Quinta-Feira, 24 de Novembro ","paciente":"edvaldo", "atendimento":"14:30"},
	                   {"dia":"Sábado, 26 de Novembro ","paciente":"edvaldo", "atendimento":"09:30"}]; 
	$scope.teste = "Ok";
	
})
.controller('indexCtrl', function($scope,$http){
	
})
.controller('calendarCtrl',function(){
	
})
.controller('pacienteCtrl',function($scope,$http){
	$scope.calendar= function($event){
		$event.preventDefault();
	    $event.stopPropagation();
	}
	
	//Busca Dinâmica de Endereço com CEP
	$scope.buscarEndereco = function(cep){
		console.log(cep);
		$http({
			method:'GET',
			url:'https://viacep.com.br/ws/'+ cep +'/json/',
			responseType:'json'
		}).success(function(response){
			console.log(response);
			$scope.endereco = response.logradouro;
		}).error(function(){
			
		})
	}
	 
      
   
	
	//Buscar Paciente p/ Nome
	$scope.buscaPaciente=function(paciente){
		$http({
			method:'GET',
			url:'/paciente',
			params:paciente
			
		}).success(function(data){
			$scope.pacientes = data;
			
			
		}).error(function(){
			
		});
	}
	//Abrir Modal de Inclusão Paciente
	$scope.openModalPac = function(){
		$("#myModal").modal($scope.modal=true);
		
	}
});