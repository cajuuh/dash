app.controller('maincontroller', [
		'$scope',
		'$mdDialog',
		'$http',
		function($scope, $mdDialog, $http) {

			$scope.consultas = [];
			$http({
				method : 'GET',
				url : '/paciente'
			}).then(function successCallback(response) {
				console.log(response);
				$scope.consultas = response.data;
				$scope.clientes = response.data;

			}, function errorCallback(response) {

			});
			$scope.title = 'Aqui!';
			$scope.clientes = [ {
				name : 'Edvaldo',
				postname : 'Azevedo',
				email : 'edper@email.com',
				phone : '+55(83)3000-0000',
				date : Date.now(),
				status : 'fa fa-circle text-info'
			}, {
				name : 'Pedro',
				postname : 'Alcântara',
				email : 'pedralc@email.com',
				phone : '+55(83)3000-0000',
				date : new Date('2015', '10', '09'),
				status : 'fa fa-circle text-danger'
			}, {
				name : 'Lauro',
				postname : 'Perazzo',
				email : 'lauper@email.com',
				phone : '+55(83)3000-0000',
				date : new Date('2015', '11', '21'),
				status : 'fa fa-circle text-danger'
			}, {
				name : 'Ani',
				postname : 'Cirne',
				email : 'anicir@email.com',
				phone : '+55(83)3000-0000',
				date : new Date('2015', '11', '22'),
				status : 'fa fa-circle text-danger'
			}, {
				name : 'Sara',
				postname : 'Silva',
				email : 'sarsil@email.com',
				phone : '+55(83)3000-0000',
				date : new Date('2015', '11', '18'),
				status : 'fa fa-circle text-danger'
			} ];

			// Datepicker
			$scope.myDate = new Date();

			$scope.minDate = new Date($scope.myDate.getFullYear(),
					$scope.myDate.getMonth() - 2, $scope.myDate.getDate());

			$scope.maxDate = new Date($scope.myDate.getFullYear(),
					$scope.myDate.getMonth() + 2, $scope.myDate.getDate());

			$scope.onlyWeekendsPredicate = function(date) {
				var day = date.getDay();
				return day === 0 || day === 6;
			};
			// ------

			// Dialog Popup
			$scope.status = '  ';
			$scope.customFullscreen = false;
			$scope.showPrompt = function(ev) {
				// Appending dialog to document.body to cover sidenav in docs
				// app
				var confirm = $mdDialog.prompt().title(
						'What would you name your dog?').textContent(
						'Bowser is a common name.').placeholder('Dog name')
						.ariaLabel('Dog name').initialValue('Buddy')
						.targetEvent(ev).ok('Okay!')
						.cancel('I\'m a cat person');

				$mdDialog.show(confirm).then(
						function(result) {
							$scope.status = 'You decided to name your dog '
									+ result + '.';
						}, function() {
							$scope.status = 'You didn\'t name your dog.';

						});
			};

			$scope.showAdvanced = function(ev) {
				$mdDialog.show({
					controller : DialogController,
					templateUrl : 'form1.tmpl.html',
					parent : angular.element(document.body),
					targetEvent : ev,
					clickOutsideToClose : true,
					fullscreen : $scope.customFullscreen
				// Only for -xs, -sm breakpoints.
				}).then(
						function(answer) {
							$scope.status = 'You said the information was "'
									+ answer + '".';
						}, function() {
							$scope.status = 'You cancelled the dialog.';
						});
			};

			$scope.showAdvanced2 = function(ev) {
				$mdDialog.show({
					controller : DialogController,
					templateUrl : 'form2.tmpl.html',
					parent : angular.element(document.body),
					targetEvent : ev,
					clickOutsideToClose : true,
					fullscreen : $scope.customFullscreen
				// Only for -xs, -sm breakpoints.
				}).then(
						function(answer) {
							$scope.status = 'You said the information was "'
									+ answer + '".';
						}, function() {
							$scope.status = 'You cancelled the dialog.';
						});
			};

			function DialogController($scope, $mdDialog) {
				$scope.hide = function() {
					$mdDialog.hide();
				};

				$scope.cancel = function() {
					$mdDialog.cancel();
				};

				$scope.answer = function(answer) {
					$mdDialog.hide(answer);
				};
			}

			/*
			 * $scope.realizarAgendamento = function(agendamento){
			 * console.log(agendamento.nome); }
			 */

			// Busca Paciente P/Nome
			$scope.buscarPaciente = function(paciente) {
				$scope.findPacientes = []; 
				console.log(paciente);
				$http({
					method:'GET',
					url:'/paciente/nome/' + paciente,
					
				}).success(function(response){
					console.log(response);
					$scope.findPacientes = response;
					console.log($scope.findPacientes);
					
				}).error(function(){
					
				});

			}

		} ]);
