package br.com.sl7.projeto.fisio.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="agendamento")
public class Agendamento implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8840975201604242088L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JoinColumn(name = "agendamento_id")
	private long id;
	@OneToOne
    @JoinColumn(name = "paciente_id")
	private Paciente paciente;
	@Temporal(TemporalType.DATE)
	private Date data;
	@Temporal(TemporalType.TIME)
	@Column(unique=true )
	private Date hora;
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "servico_id")
	private Servico servico;
	private String anotacao;

	

	public Agendamento() {
		
	}

	

	public Agendamento(Paciente paciente, Date data,Date hora, Servico servico,String anotacao) {
		this.paciente = paciente;
		this.data = data;
		this.servico = servico;
		this.hora = hora;
		this.anotacao = anotacao;
	}
	

	public long getId() {
		return id;
	}
	

	public void setId(long id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getAnotacao() {
		return anotacao;
	}

	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	
	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}



	@Override
	public String toString() {
		return "Agendamento [id=" + id + ", paciente=" + paciente.toString() + ", data=" + data + ", hora=" + hora + ", servico="
				+ servico.toString() + ", anotacao=" + anotacao + "]";
	}

	
	
}
