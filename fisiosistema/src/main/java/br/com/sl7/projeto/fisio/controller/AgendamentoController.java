package br.com.sl7.projeto.fisio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sl7.projeto.fisio.domain.Agendamento;
import br.com.sl7.projeto.fisio.domain.Paciente;
import br.com.sl7.projeto.fisio.excecoes.AgendamentoExisteDataHoraException;
import br.com.sl7.projeto.fisio.service.AgendamentoService;
import br.com.sl7.projeto.fisio.service.PacienteService;

@RestController
@RequestMapping(value = "/agendamento")
public class AgendamentoController {

	@Autowired
	private AgendamentoService service;
	@Autowired
	private PacienteService pacienteService;
	private List<Agendamento> agendamento;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> criarAgendamento(@RequestBody Agendamento agendamento) throws AgendamentoExisteDataHoraException{
		try {
			Agendamento agend = service.findPorDataeHora(agendamento.getData(), agendamento.getHora());
			if (agend == null) {
				service.cadastrarAgendamento(agendamento);
				return new ResponseEntity<String>(HttpStatus.CREATED);
			} else {
				return new ResponseEntity<String>( new AgendamentoExisteDataHoraException().getMessage(), HttpStatus.CONFLICT);
			}

		} catch (Exception e) {
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Agendamento>> todosAgendamentos() {
		try {
			List<Agendamento> agendamentos = service.agendamentos();
			return new ResponseEntity<List<Agendamento>>(agendamentos, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/nome/{paciente}", method = RequestMethod.GET)
	public ResponseEntity<List<Agendamento>> buscarAgendamentoPorPaciente(@PathVariable(value = "paciente") String paciente) {
		
		Paciente paciente1 = (Paciente) pacienteService.pacientePorNome1(paciente);
		
		if(paciente1 == null){
			System.out.println("Paciente Não encontrado");
		}else{
			agendamento = service.agendamentoPorPaciente(paciente1);
		}
		System.out.println(paciente1.getNome());
		System.out.println(agendamento);
		return new ResponseEntity<List<Agendamento>>(agendamento, HttpStatus.OK);

	}
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<String> deletarAgendamento(@PathVariable(value="id")Long id){
		try {
			service.deletarAgendamento(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		
	}

}
