package br.com.sl7.projeto.fisio.excecoes;

public class AgendamentoExisteDataHoraException  extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8627746503621514652L;
	
	public AgendamentoExisteDataHoraException() {
		
		System.out.println("Já existe agendamento para esta data e hora!");
	}
}
