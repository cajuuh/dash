package br.com.sl7.projeto.fisio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.sl7.projeto.fisio.domain.Servico;

public interface ServicoRepository extends JpaRepository<Servico, Long> {

}
