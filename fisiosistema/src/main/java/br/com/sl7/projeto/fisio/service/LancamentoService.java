package br.com.sl7.projeto.fisio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sl7.projeto.fisio.domain.Lancamento;
import br.com.sl7.projeto.fisio.repository.LancamentoRepository;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository lancamentoRepository;

	public void realizarLancamento(Lancamento lancamento) {
		lancamentoRepository.save(lancamento);

	}

}
