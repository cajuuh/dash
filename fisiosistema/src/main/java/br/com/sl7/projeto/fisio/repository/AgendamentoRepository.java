package br.com.sl7.projeto.fisio.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.sl7.projeto.fisio.domain.Agendamento;

public interface AgendamentoRepository extends JpaRepository<Agendamento, Long> {
	

	Agendamento findByHora(Date hora);

	Agendamento findByDataAndHora(Date data, Date hora);

	List<Agendamento> findByPaciente_id(long id);


}
