package br.com.sl7.projeto.fisio.excecoes;

public class UsuarioInvalidoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8548754362498979444L;
	
	public UsuarioInvalidoException(String msg) {
		super(msg);
	}

}
