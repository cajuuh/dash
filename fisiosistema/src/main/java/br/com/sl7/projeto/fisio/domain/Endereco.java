package br.com.sl7.projeto.fisio.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Endereco implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2769932413897802784L;
	private String logradouro;
	private String bairro;
	private String cep;
	private String localidade;

	
	public Endereco() {
		
	}

	public Endereco(String logradouro, String bairro, String cep, String localidade) {
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.cep = cep;
		this.localidade = localidade;
		
	}

	

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	@Override
	public String toString() {
		return "Endereco [logradouro=" + logradouro + ", bairro=" + bairro + ", cep=" + cep + ", localidade="
				+ localidade + "]";
	}

	
}
