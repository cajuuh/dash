package br.com.sl7.projeto.fisio.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sl7.projeto.fisio.domain.Agendamento;
import br.com.sl7.projeto.fisio.domain.Paciente;
import br.com.sl7.projeto.fisio.repository.AgendamentoRepository;

@Service
public class AgendamentoService {
	
	@Autowired
	private AgendamentoRepository repository;

	public Agendamento agendamentoPorData(Date hora) {
		return repository.findByHora(hora);
	}

	public List<Agendamento> agendamentoPorPaciente(Paciente paciente) {
		
		return repository.findByPaciente_id(paciente.getId());
	
	}

	public void cadastrarAgendamento(Agendamento agendamento) {
		
		repository.save(agendamento);
	}

	public List<Agendamento> agendamentos() {
		
		return repository.findAll();
	}

	public Agendamento findPorDataeHora(Date data, Date hora) {
		Agendamento ag = repository.findByDataAndHora(data,hora);
		return ag;
	}

	public void deletarAgendamento(Long id) {
		repository.delete(id);
		
	}

	
	
	

}
