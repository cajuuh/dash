package br.com.sl7.projeto.fisio.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.dom4j.tree.AbstractEntity;

@Entity
public class Usuario extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7469638699232651740L;

	public Usuario() {
	
	}

	public Usuario(long id, String nome, String senha) {

		this.id = id;
		this.nome = nome;
		this.senha = senha;
	}

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String nome;
	private String senha;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
