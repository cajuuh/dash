package br.com.sl7.projeto.fisio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication

public class Main extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}
	protected final SpringApplicationBuilder configure(final SpringApplicationBuilder main) {
        return main.sources(Main.class);
    }
	
	
	

}
