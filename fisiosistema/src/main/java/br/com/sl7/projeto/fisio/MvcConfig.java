package br.com.sl7.projeto.fisio;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig  extends WebMvcConfigurerAdapter  {
	
	@Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/dashboard").setViewName("dashboard.html");
        registry.addViewController("/index").setViewName("dashboard.html");
        registry.addViewController("/login").setViewName("login.html");
    }

}
