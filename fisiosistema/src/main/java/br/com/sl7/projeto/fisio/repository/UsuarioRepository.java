package br.com.sl7.projeto.fisio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.sl7.projeto.fisio.domain.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	public Usuario findByNomeAndSenha(String nome,String senha);

	public Usuario findByNome( String nome);
}
