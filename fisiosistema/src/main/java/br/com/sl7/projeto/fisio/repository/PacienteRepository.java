package br.com.sl7.projeto.fisio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.sl7.projeto.fisio.domain.Paciente;

public interface PacienteRepository extends JpaRepository<Paciente,Long> {
	
	@Query("SELECT p FROM Paciente p WHERE UPPER(p.nome) LIKE CONCAT(UPPER(:nome),'%')")
	List<Paciente> findByNome(@Param("nome")String paciente);
	@Query("SELECT p FROM Paciente p WHERE UPPER(p.nome) LIKE CONCAT(UPPER(:paciente),'%')")
	Paciente findByNome1(@Param("paciente")String paciente);


}
