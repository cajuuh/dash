package br.com.sl7.projeto.fisio.service;

import java.util.List;

import br.com.sl7.projeto.fisio.repository.ServicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sl7.projeto.fisio.domain.Servico;
import br.com.sl7.projeto.fisio.repository.ServicoRepository;

@Service
public class ServicoService {
	
	@Autowired 
	private ServicoRepository servicoRepository;

	public List<Servico> buscarServicos() {
		
		return servicoRepository.findAll();
	}

	public void cadastrarServico(Servico servico) {
		
		servicoRepository.save(servico);
		
	}
}
