package br.com.sl7.projeto.fisio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sl7.projeto.fisio.domain.Paciente;
import br.com.sl7.projeto.fisio.service.PacienteService;

@RestController
@RequestMapping(value = "/paciente")
public class PacienteController {
	@Autowired
	private PacienteService service;
	private Paciente paciente;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Paciente>> todosPacientes() {
		try {
			List<Paciente> pacientes = service.buscarPacientes();
			return new ResponseEntity<List<Paciente>>(pacientes, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Paciente>>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}
	@RequestMapping(value="/nome/{nome}", method=RequestMethod.GET)
	public ResponseEntity<List<Paciente>> todosPacientesPorNome(@PathVariable(value="nome")  String nome) {
		try {
			List<Paciente> pacientes = (List<Paciente>) service.pacientePorNome(nome);
			return new ResponseEntity<List<Paciente>>(pacientes, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Paciente>>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Paciente> buscarPaciente(@PathVariable long id) {
		try {
			paciente = service.pacientePorId(id);
			return new ResponseEntity<Paciente>(paciente, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Paciente>(HttpStatus.NOT_FOUND);

		}

	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> criarPaciente(@RequestBody Paciente paciente) {
		try {
			service.salvarPaciente(paciente);
			return new ResponseEntity<String>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<String> atualizarPaciente(@RequestBody Paciente paciente) {
		try {
			service.atualizarPaciente(paciente);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> removerPaciente(@PathVariable(value="id")  Long id) {
		try {
			service.deletarPaciente(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}

	}
	

}
