package br.com.sl7.projeto.fisio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import br.com.sl7.projeto.fisio.service.UsuarioDetailsService;
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter  {

	@Autowired
	private UsuarioDetailsService detailsService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(detailsService);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
		.antMatchers("/resources/public/**").permitAll()
        .antMatchers("/resources/img/**").permitAll()
        .antMatchers("/resources/bower_components/**").permitAll()
		.antMatchers("/home.html","/user.html","/dashboard.html","/paciente","/produto","/agendamento","/servico").authenticated()
		.and().csrf().disable()
		.formLogin()
			.loginPage("/login")
			.defaultSuccessUrl("/dashboard")
			.failureUrl("/login")
			.and()
				.logout()
                .deleteCookies("JSESSIONID","USER")
                .logoutSuccessUrl("/login")
            .permitAll();
		
		  http
          .sessionManagement()
          .maximumSessions(3)
          .expiredUrl("/login")
          .maxSessionsPreventsLogin(true)
          .and()
          .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
          .invalidSessionUrl("/login");
		  
		 


	}
	
	
	
	



}
