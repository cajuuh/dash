package br.com.sl7.projeto.fisio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import br.com.sl7.projeto.fisio.domain.Paciente;
import br.com.sl7.projeto.fisio.repository.PacienteRepository;

@Service
public class PacienteService {
	@Autowired
	private PacienteRepository repository;
	public List<Paciente> buscarPacientes() {
		return repository.findAll();
	}

	public Paciente pacientePorId(long id) {

		return repository.findOne(id);
	}
	
	public void salvarPaciente(Paciente paciente) {
		repository.save(paciente);
	}

	@Modifying
	public void atualizarPaciente(Paciente paciente) {
		repository.save(paciente);
	}

	public void deletarPaciente(long id) {
		repository.delete(id);

	}

	public List<Paciente> pacientePorNome(String nome) {
		
		return repository.findByNome(nome);
	}
	public Paciente pacientePorNome1(String paciente){
		return repository.findByNome1(paciente);
	}

}
