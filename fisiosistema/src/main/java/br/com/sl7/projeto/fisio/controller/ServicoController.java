package br.com.sl7.projeto.fisio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sl7.projeto.fisio.domain.Servico;
import br.com.sl7.projeto.fisio.service.ServicoService;

@RestController
@RequestMapping(value="/servico")
public class ServicoController {
	@Autowired
	private ServicoService servicoService;
	private List<Servico> servicos;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> criarServico(@RequestBody Servico servico) {
		try {
			servicoService.cadastrarServico(servico);
			return new ResponseEntity<String>("Cadastrado com Sucesso", HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<String>("Falha ao Cadastrar Servico", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Servico>> todosServicos() {
		try {
			servicos = servicoService.buscarServicos();
			return new ResponseEntity<List<Servico>>(servicos, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<Servico>>(servicos, HttpStatus.NOT_FOUND);
		}

	}

}
