package br.com.sl7.projeto.fisio.domain;

import java.io.Serializable;
import java.util.Date;

public class Sessao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5184649135796875633L;
	private int quantSessao;
	private Servico servico;
	private Date dataSessao;

	public int getQuantSessao() {
		return quantSessao;
	}

	public void setQuantSessao(int quantSessao) {
		this.quantSessao = quantSessao;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Date getDataSessao() {
		return dataSessao;
	}

	public void setDataSessao(Date dataSessao) {
		this.dataSessao = dataSessao;
	}

}
