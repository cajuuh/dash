package br.com.sl7.projeto.fisio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sl7.projeto.fisio.domain.Lancamento;
import br.com.sl7.projeto.fisio.service.LancamentoService;

@RestController
@RequestMapping(value = "/lancamento")
public class LancamentoController {

	@Autowired
	private LancamentoService lancamentoService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> criarLancamento(@RequestBody Lancamento lancamento) {

		try {
			lancamentoService.realizarLancamento(lancamento);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
