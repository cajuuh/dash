package br.com.sl7.projeto.fisio.domain;

public enum TipoTelefone {
	residencial, celular, trabalho, comercial, whatsapp

}
