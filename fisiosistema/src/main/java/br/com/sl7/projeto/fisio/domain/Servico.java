package br.com.sl7.projeto.fisio.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Servico  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -330328669474417459L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "servico_id")
	private long id;
	private String descricao;
	private String tipo;
	private double valor;
	private int duracao;
	
	public Servico() {
		
	}
	
	public Servico(String descricao, double valor, int duracao,String tipo) {
		this.setTipo(tipo);
		this.descricao = descricao;
		this.valor = valor;
		this.duracao = duracao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Servico [id=" + id + ", descricao=" + descricao + ", valor=" + valor + ", duracao=" + duracao + "]";
	}
	
	

}
