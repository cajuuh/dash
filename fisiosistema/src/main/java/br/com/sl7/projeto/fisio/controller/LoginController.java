package br.com.sl7.projeto.fisio.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/autenticar")
public class LoginController {
	
	@RequestMapping(method= RequestMethod.GET)
	public String loginPage(){
		return "login";
	}
	

}
