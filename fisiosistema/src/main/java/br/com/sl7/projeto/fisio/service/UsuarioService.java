package br.com.sl7.projeto.fisio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sl7.projeto.fisio.domain.Usuario;
import br.com.sl7.projeto.fisio.repository.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired
	private UsuarioRepository repository;

	public Usuario cadastrar(Usuario usu) {
		return repository.save(usu);
	}

	public List<Usuario> buscarUsuarios() {
		return repository.findAll();
	}

	public Usuario validarUsuario(String nome, String senha) {

		Usuario u = repository.findByNomeAndSenha(nome, senha);

		System.out.println("Nome: " + u.getNome() + "Senha: " + u.getSenha());
		return u;

	}

}
