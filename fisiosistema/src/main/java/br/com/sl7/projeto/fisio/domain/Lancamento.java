package br.com.sl7.projeto.fisio.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Lancamento {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long codigo;
	private Agendamento agendamento;
	private Date dataLancamento;
	private String tipoPagamento;
	private int desconto;

	public Lancamento() {

	}

	public Lancamento(Agendamento agendamento, Date dataLancamento, String tipoPagamento, int desconto) {
		this.agendamento = agendamento;
		this.dataLancamento = dataLancamento;
		this.tipoPagamento = tipoPagamento;
		this.desconto = desconto;
	}

	public Agendamento getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(Agendamento agendamento) {
		this.agendamento = agendamento;
	}

	public Date getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public String getTipoPagamento() {
		return tipoPagamento;
	}

	public void setTipoPagamento(String tipoPagamento) {
		this.tipoPagamento = tipoPagamento;
	}

	public int getDesconto() {
		return desconto;
	}

	public void setDesconto(int desconto) {
		this.desconto = desconto;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

}
