package br.com.sl7.projeto.fisio.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class Telefone implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4773617577055484112L;
	
	private String tipo;
	private String numero;

	public Telefone() {

	}

	public Telefone(String tipo, String numero) {

		this.tipo = tipo;
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Telefone [tipo=" + tipo + ", numero=" + numero + "]";
	}
	
	
}
