package br.com.sl7.projeto.fisio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sl7.projeto.fisio.domain.Usuario;
import br.com.sl7.projeto.fisio.service.UsuarioService;

@RestController
@RequestMapping(value = "/usuario")
public class UsuarioController {
	@Autowired
	private UsuarioService service;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Usuario>> listarTodos() {

		return new ResponseEntity<List<Usuario>>(service.buscarUsuarios(), HttpStatus.OK);

	}

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<String> criarUsuario(@RequestBody Usuario usuario) {
		try {
			service.cadastrar(usuario);
			return new ResponseEntity<String>(HttpStatus.CREATED);
		} catch (Exception e) {

		}
		return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
