app.controller('maincontroller', ['$scope', '$mdDialog', function($scope,$mdDialog) {

  $scope.title = "Mariá Dermato Funcional";
  
  $scope.cliente =
    {
      tipo: "Paciente",
			name: '',
			postname: '',
      address: '',
      city: '',
      cep: '',
			email: '',
			phone: '',
			date: Date.now(),
			status: 'fa fa-circle text-info',
      notations: ""
    };

	$scope.clientes = [
		{
      tipo: "Paciente",
			name: 'Edvaldo',
			postname: 'Azevedo',
			email: 'edaz@email.com',
			phone: '+55(83)3000-0000',
			date: Date.now(),
			status: 'fa fa-circle text-info'
		},
		{
      tipo: "Paciente",
			name: 'Pedro',
			postname: 'Alcântara',
			email: 'pedralc@email.com',
			phone: '+55(83)3000-0000',
			date: new Date('2015', '10', '09'),
			status:'fa fa-circle text-danger'
		},
		{
      tipo: "Paciente",
			name: 'Lauro',
			postname: 'Perazzo',
			email: 'lauper@email.com',
			phone: '+55(83)3000-0000',
			date: new Date('2015', '11', '21'),
			status: 'fa fa-circle text-danger'
		},
		{
      tipo: "Paciente",
			name: 'Ani',
			postname: 'Cirne',
			email: 'anicir@email.com',
			phone: '+55(83)3000-0000',
			date: new Date('2015', '11', '22'),
			status: 'fa fa-circle text-danger'
		}
	];

	$scope.consultas =[
		{
			name: 'Edvaldo',
			postname: 'Pereira',
			date: Date.now(),
			status: 'fa fa-circle text-info'
		},
		{
			name: 'Pedro',
			postname: 'Alcântara',
			date: new Date('2014', '03', '09'),
			status:'fa fa-circle text-danger'
		},
		{
			name: 'Lauro',
			postname: 'Perazzo',
			date: new Date('2014', '03', '10'),
			status: 'fa fa-circle text-danger'
		}
	];

  $scope.getInterest1 = function() {
      var results = $scope.clientes;
      console.log(results);
      return results;
  };

	
  // ------ Duration Picker  ------ 
  $scope.items = [1, 2, 3];
  $scope.selectedHour;
  $scope.getSelectedHour = function() {
    if ($scope.selectedHour !== undefined) {
      return "Você selecionou " + $scope.selectedHour + " hora(s)";
    } else {
      return "Por favor selecione a duração";
    }
  };
  // ------ Duration Picker  ------

  // ------ Service Picker  ------
  $scope.services = [1, 2, 3];
  $scope.selectedService;
  $scope.getSelectedService = function() {
    if ($scope.selectedService !== undefined) {
      return "Você selecionou o Serviço " + $scope.selectedService;
    } else {
      return "Por favor selecione o Serviço";
    }
  };
  // ------ Service Picker  ------

}]);

app.controller('calendarcontroller', function CalendarCtrl($scope,$filter,$compile,uiCalendarConfig,$mdDialog) {
	
  angular.element('maincontroller').scope(title);
    
  $scope.calendarDate = [
		{
			events: [
				{
					title: 'From',
					start: '2015-11-11',
					allDay: true,
					rendering: 'background',
					backgroundColor: '#f26522',
				},
			],
		}
	];
	
  	var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
	  var myCalendar = uiCalendarConfig.calendars.myCalendar;
    
    $scope.changeTo = 'Hungarian';
    /* event source that pulls from google.com */
    $scope.eventSource = {
            url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
            className: 'gcal-event',           // an option!
    };
	
    /* event source that contains custom events on the scope */
    $scope.events = [
      {title: 'All Day Event',start: new Date(y, m, 1),allDay: true},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
      {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
      {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
      {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'},
		  {title: 'My Event',start: new Date(2016,11,9,16,0),allDay: false},
      {title: 'My Event3',start: new Date(2016,11,9,18,0),allDay: false},
      {title: 'My Event4',start: new Date(2016,11,9,22,0),allDay: false},
      {title: 'My Event2',start: new Date(2016,11,9,15,30),allDay: false}
    ];
	
    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
      var s = new Date(start).getTime() / 1000;
      var e = new Date(end).getTime() / 1000;
      var m = new Date(start).getMonth();
      var events = [{title: 'Feed Me ' + m,start: s + (50000),end: s + (100000),allDay: false, className: ['customFeed']}];
      callback(events);
    };

    $scope.calEventsExt = {
       color: '#f00',
       textColor: 'yellow',
       events: [ 
          {type:'party',title: 'Lunch',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Lunch 2',start: new Date(y, m, d, 12, 0),end: new Date(y, m, d, 14, 0),allDay: false},
          {type:'party',title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
        ]
    };
    /* alert on eventClick */
    $scope.alertOnEventClick = function( date, jsEvent, view){
        $scope.alertMessage = (date.title + ' was clicked ');
    };
    /* alert on Drop */
     $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
       $scope.alertMessage = ('Event Droped to make dayDelta ' + delta);
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
       $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources,source) {
      var canAdd = 0;
      angular.forEach(sources,function(value, key){
        if(sources[key] === source){
          sources.splice(key,1);
          canAdd = 1;
        }
      });
      if(canAdd === 0){
        sources.push(source);
      }
    };
    /* add custom event*/
    $scope.addEvent = function() {
      $scope.events.push({
        title: 'Open Sesame',
        start: new Date(y, m, 28),
        end: new Date(y, m, 29),
        className: ['openSesame']
      });
    };
    /* remove event */
    $scope.remove = function(index) {
      $scope.events.splice(index,1);
    };
    /* Change View */
    $scope.changeView = function(view,calendar) {
      uiCalendarConfig.calendars[calendar].fullCalendar('changeView',view);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
      if(uiCalendarConfig.calendars[calendar]){
        uiCalendarConfig.calendars[calendar].fullCalendar('render');
      }
    };
     /* Render Tooltip */
    $scope.eventRender = function( event, element, view ) { 
        element.attr({'tooltip': event.title,
                     'tooltip-append-to-body': true});
        $compile(element)($scope);
    };

    var splitTime = function(timesplitter){
      timesplitter.split()
    };

    $scope.setCalDate = function(date, jsEvent, view) {

        var selectedDate = moment(date).format('D');				    // set dateFrom based on user click on calendar
				var log = [];
				var title;
        $scope.calendarDate[0].events[0].start = selectedDate;				    // update Calendar event dateFrom
        $scope.selectedDate = $filter('date')(selectedDate, 'yyyy-MM-dd');		// update $scope.dateFrom
        //console.log('$scope.uiConfig', $scope.uiConfig);
        //console.log('uiCalendarConfig', uiCalendarConfig);
        angular.forEach($scope.events, function(value, key){
          if(value.start.getDate() == selectedDate)
            log.push(value.title);
        });
        
				if(log[0] == null){
					console.log("");
				}
				else{
					$mdDialog.show({
					 parent: angular.element(document.body),
           targetEvent: jsEvent,
           template:
           '<md-dialog aria-label="List dialog">' +
           '  <md-dialog-content>'+
           '    <md-list>'+
           '      <md-list-item ng-repeat="item in log">'+
           '       <p>{{item}}</p>' +
           '      '+
           '    </md-list-item></md-list>'+
           '  </md-dialog-content>' +
           '  <md-dialog-actions>' +
           '    <md-button ng-click="closeDialog()" class="md-primary">' +
           '      Ok' +
           '    </md-button>' +
           '  </md-dialog-actions>' +
           '</md-dialog>',
           locals: {
             log: log
            },
            controller: DialogController

          });
          function DialogController($scope, $mdDialog, log) {
            $scope.log = log;
            $scope.closeDialog = function() {
              $mdDialog.hide();
            }
          }
				}
				
      };
	
    $scope.showAlert = function(date, jsEvent, view) {
    // Appending dialog to document.body to cover sidenav in docs app
    // Modal dialogs should fully cover application
    // to prevent interaction outside of dialog
    $mdDialog.show(
      $mdDialog.alert()
        .clickOutsideToClose(true)
        .title(date.title)
        .textContent(date.start.format('h:mmA'))
        .ariaLabel('Event Dialog')
        .ok('Ok')
        .targetEvent(jsEvent)
    );
  };
    /* config object */
    $scope.uiConfig = {
      calendar:{
        lang: 'pt-br',
        height: 450,
        aspectRatio: 200,
        editable: true,
        header:{
          left: 'title',
          center: '',
          right: 'today prev,next'
        },
        dayClick: $scope.setCalDate,
        eventClick: $scope.showAlert,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender
      }
    };

    $scope.changeLang = function() {
      if($scope.changeTo === 'Hungarian'){
        $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
        $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
        $scope.changeTo= 'English';
      } else {
        $scope.uiConfig.calendar.dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        $scope.uiConfig.calendar.dayNamesShort = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        $scope.changeTo = 'Hungarian';
      }
    };
    /* event sources array*/
    $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
    $scope.eventSources2 = [$scope.calEventsExt, $scope.eventsF, $scope.events];

});



app.controller('formscontroller', ['$scope', '$mdDialog', function($scope,$mdDialog) {
  // ------ Dialog Popup  ------ 

	$scope.status = '  ';
	$scope.customFullscreen = false;
  $scope.pacienteName = '';

  $scope.getSelectedValue = function(name){
    pacienteName = name;
    console.log(pacienteName);
  };

  //Dialog pacient search
	$scope.showAdvanced = function(ev) {
		$mdDialog.show({
		  controller: DialogController,
		  templateUrl: 'form1.tmpl.html',
		  parent: angular.element(document.body),
		  targetEvent: ev,
		  clickOutsideToClose:true,
		  fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
		});
  	};
	
  //Dialog pacient sign-up
	$scope.showAdvanced2 = function(ev) {
		$mdDialog.show({
		  controller: DialogController,
		  templateUrl: 'form2.tmpl.html',
		  parent: angular.element(document.body),
		  targetEvent: ev,
		  clickOutsideToClose:true,
		  fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
		});
  	};

    //Dialog appointment
    $scope.showAdvanced3 = function(ev) {
      $mdDialog.show({
        controller: DialogController,
        templateUrl: 'form3.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
      });
  	};
   
  //"Controller" to custom dialog
  function DialogController($scope, $mdDialog) {
    $scope.hide = function() {
      $mdDialog.hide();
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };
  }
  // ------ Dialog Popup  ------

  // ------ Duration Picker  ------ 
  $scope.items = [1, 2, 3];
  $scope.selectedHour;
  $scope.getSelectedHour = function() {
    if ($scope.selectedHour !== undefined) {
      return "Você selecionou " + $scope.selectedHour + " hora(s)";
    } else {
      return "Por favor selecione a duração";
    }
  };
  // ------ Duration Picker  ------

  // ------ Service Picker  ------
  $scope.services = [1, 2, 3];
  $scope.selectedService;
  $scope.getSelectedService = function() {
    if ($scope.selectedService !== undefined) {
      return "Você selecionou o Serviço " + $scope.selectedService;
    } else {
      return "Por favor selecione o Serviço";
    }
  };
  // ------ Service Picker  ------
}]);

app.controller('pickerscontroller', ['$scope', '$mdDialog', function($scope,$mdDialog) {

}]);

angular.module('myApp').controller('panelcontroller', function ($scope) {
  $scope.accordianData = [
		{
			"content" : "Informações a mais sobre o paciente."
		}
	];
	
	$scope.collapseAll = function(data) {
	   for(var i in $scope.accordianData) {
		   if($scope.accordianData[i] != data) {
			   $scope.accordianData[i].expanded = false;   
		   }
	   }
	   data.expanded = !data.expanded;
	};
});