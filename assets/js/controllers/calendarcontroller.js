angular.module('calendarApp').controller('calendar-ontroller', function CalendarCtrl($scope,$compile,uiCalendarConfig) {
    
    $scope.calendarDate = [
		{
			events: [
				{
					title: 'From',
					start: '2015-01-31',
					allDay: true,
					rendering: 'background',
					backgroundColor: '#f26522',
				},
			],
		}
	];

        $scope.setCalDate = function(date, jsEvent, view) {
            var selectedDate = moment(date).format('YYYY-MM-DD');				    // set dateFrom based on user click on calendar
            $scope.calendarDate[0].events[0].start = selectedDate;				    // update Calendar event dateFrom
            $scope.selectedDate = $filter('date')(selectedDate, 'yyyy-MM-dd');;		// update $scope.dateFrom
                console.log('$scope.uiConfig', $scope.uiConfig);
                console.log('uiCalendarConfig', uiCalendarConfig);
	};

    $scope.uiConfig = {
		calendar : {
			editable : false,
        aspectRatio: 2,
			header : {
				left : 'title',
				center : '',
				right : 'today prev,next'
			},
			dayClick : $scope.setCalDate,
			background: '#f26522',
		},
	};

});